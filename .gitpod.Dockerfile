FROM gitpod/workspace-full:2022-05-08-14-31-53

RUN sudo install-packages php-xdebug

# vue
RUN npm i -g vue-cli

# mysql
USER gitpod
RUN sudo apt update && sudo rm -rf /var/lib/apt/lists/*
